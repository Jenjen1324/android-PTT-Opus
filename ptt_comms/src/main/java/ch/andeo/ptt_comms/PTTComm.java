package ch.andeo.ptt_comms;

public interface PTTComm {

    void ping();

    void audioFrame(byte[] frame);

}
