package ch.andeo.ptt_comms;

import java.util.Arrays;

public class PTTCommReceiver {

    private PTTComm listener;

    public PTTCommReceiver(PTTComm listener) {
        this.listener = listener;
    }

    public void onData(byte[] data) {

        if (ByteUtils.startsWith(data, "REG".getBytes())) {
            listener.ping();
        } else if (ByteUtils.startsWith(data, "AF".getBytes())) {
            listener.audioFrame(Arrays.copyOfRange(data, 2, data.length));
        }

    }

}
