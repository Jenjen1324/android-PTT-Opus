package ch.andeo.ptt_comms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class PTTCommSender implements PTTComm {

    private DatagramSocket socket;
    private int maxBufferSize;

    public PTTCommSender(DatagramSocket socket, int maxBufferSize) {
        this.socket = socket;
        this.maxBufferSize = maxBufferSize;
    }

    private void send(String data) {
        send(data.getBytes());
    }

    private void send(byte[] bytes) {
        if (bytes.length > maxBufferSize) {
            throw new RuntimeException("Frame exceeds maxBufferSize! Got " + bytes.length + " bytes");
        }

        DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
        try {
            socket.send(packet);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public void ping() {
        send("REG");
    }

    @Override
    public void audioFrame(byte[] frame) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            os.write("AF".getBytes());
            os.write(frame);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        send(os.toByteArray());
    }
}
