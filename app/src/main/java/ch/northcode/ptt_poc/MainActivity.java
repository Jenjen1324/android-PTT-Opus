package ch.northcode.ptt_poc;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    BufferedConsumer consumer;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AudioSessionConfig cfg = AudioSessionConfig.getFastConfig();
        //consumer = new BufferedConsumer();

        Player player = new Player(cfg);
        UdpComms comms;
        try {
            InetAddress addr = InetAddress.getByName("10.0.2.189");
            comms = new UdpComms(cfg, addr, 8884, player);
            comms.init();
            comms.listen();
        } catch (SocketException | UnknownHostException e) {
            Log.e(TAG, "Error", e);
            throw new RuntimeException(e);
        }

        Recorder recorder = new Recorder(cfg, comms);

        findViewById(R.id.button_ptt)
                .setOnTouchListener((view, motionEvent) -> {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        Log.v(TAG, "Touchy down");
                        //consumer.reset();
                        recorder.start();
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        Log.v(TAG, "Touchy up");
                        recorder.end();
                    }

                    return false;
                });


        findViewById(R.id.button_play)
                .setOnClickListener(view -> {
                    //consumer.play(player);
                });
    }



    private static class BufferedConsumer implements Recorder.AudioConsumer {

        private List<byte[]> buffer = new ArrayList<>();

        void reset() {
            buffer.clear();
        }

        void play(Player player) {
            for (byte[] bytes : buffer) {
                player.onData(bytes);
            }
            buffer.clear();
        }

        @Override
        public void onAudio(byte[] data) {
            buffer.add(data);
        }
    }
}
