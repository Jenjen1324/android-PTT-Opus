package ch.northcode.ptt_poc;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ch.andeo.ptt_comms.PTTComm;

/**
 * Created by Jens on 04.07.2018.
 */

public class UdpComms implements Recorder.AudioConsumer, AutoCloseable {

    private static final String TAG = "UdpComms";

    private final AudioSessionConfig cfg;

    private Player player;
    private DatagramSocket socket;
    private InetAddress target;
    private int port;

    private ExecutorService executorService;
    private ExecutorService pingSerivce;

    private boolean isRunning = true;

    public UdpComms(AudioSessionConfig cfg, InetAddress target, int port, Player player) throws SocketException {
        this.cfg = cfg;
        this.player = player;
        this.socket = new DatagramSocket();
        this.socket.getLocalPort();
        this.target = target;
        this.port = port;

        executorService = Executors.newSingleThreadExecutor();
        pingSerivce = Executors.newSingleThreadExecutor();
    }

    public void init() {
        pingSerivce.submit(() -> {
            Thread.currentThread().setName("UDP-Ping");
            byte[] payload = "REG".getBytes();
            DatagramPacket packet = new DatagramPacket(payload, payload.length, target, port);

            try {
                while (!Thread.interrupted() && isRunning) {
                    try {
                        socket.send(packet);

                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }

                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                Log.i(TAG, "PingService interrupted");
            }
        });
    }

    public void listen() {
        executorService.submit(() -> {
            Thread.currentThread().setName("UDP-Receiver");
            byte[] buf = new byte[cfg.bufSize];
            try {
                while (!Thread.interrupted() && isRunning) {
                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
                    Log.d(TAG, "Waiting for packet...");
                    socket.receive(packet);
                    Log.v(TAG, "Received packet!");
                    try {
                        player.onData(packet.getData());
                    } catch (Exception e) {
                        Log.w(TAG, "Failed interpreting packet");
                    }
                    Log.d(TAG, "Packet interpreted");
                }
            } catch (IOException e) {
                Log.e(TAG, "Failed receiving packet", e);
            } catch (Exception e) {
                Log.e(TAG, "Listener thread failed", e);
            }
        });
    }

    @Override
    public void onAudio(byte[] data) {
        DatagramPacket packet = new DatagramPacket(data, 0, data.length, target, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            Log.e(TAG, "Failed sending data", e);
        }
    }

    @Override
    public void close() throws Exception {
        isRunning = false;
        executorService.shutdownNow();
    }

}
