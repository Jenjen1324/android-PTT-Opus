package ch.northcode.ptt_poc;

import android.media.AudioFormat;
import android.media.AudioRecord;

/**
 * Created by Jens on 02.07.2018.
 */

public class AudioSessionConfig {

    public int sampleRate;
    public int frameSize;
    public int bufSize;
    public int encoding;

    public int minBufSize(int channelConfig) {
        return AudioRecord.getMinBufferSize(sampleRate, channelConfig, encoding);
    }

    public static AudioSessionConfig getFastConfig() {
        AudioSessionConfig cfg = new AudioSessionConfig();
        cfg.sampleRate = 16000;
        cfg.frameSize = 160;
        cfg.bufSize = cfg.frameSize;
        cfg.encoding = AudioFormat.ENCODING_PCM_16BIT;

        return cfg;
    }

}
