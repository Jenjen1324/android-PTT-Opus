package ch.northcode.ptt_poc;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import com.score.rahasak.utils.OpusDecoder;

/**
 * Created by Jens on 02.07.2018.
 */

public class Player implements AutoCloseable {

    private static final String TAG = "Player";

    private final AudioSessionConfig cfg;

    private AudioTrack track;
    private OpusDecoder opusDecoder;

    private short[] pcmFrames;

    public Player(AudioSessionConfig cfg) {
        this.cfg = cfg;

        track = new AudioTrack(
                AudioManager.STREAM_MUSIC,
                cfg.sampleRate,
                AudioFormat.CHANNEL_OUT_MONO,
                cfg.encoding,
                cfg.bufSize,
                AudioTrack.MODE_STREAM
        );

        track.play();

        opusDecoder = new OpusDecoder();
        opusDecoder.init(cfg.sampleRate, 1);

        pcmFrames = new short[cfg.bufSize];
    }

    public void onData(byte[] bytes) {

        int decodedBytes = opusDecoder.decode(bytes, pcmFrames, cfg.frameSize);
        Log.v(TAG, "Decoded " + bytes.length + " bytes to " + decodedBytes + " bytes");

        track.write(pcmFrames, 0, pcmFrames.length);
    }

    @Override
    public void close() {
        opusDecoder.close();
        track.release();
    }
}
