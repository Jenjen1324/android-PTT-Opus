package ch.northcode.ptt_poc;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;

import com.score.rahasak.utils.OpusEncoder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jens on 02.07.2018.
 */

public class Recorder {

    private static final String TAG = "Recorder";

    private final AudioSessionConfig cfg;

    private static final int AUDIO_SOURCE = MediaRecorder.AudioSource.MIC;

    private AudioRecord recorder;
    private OpusEncoder encoder;

    private ExecutorService senderThread;
    private volatile boolean running = false;

    private AudioConsumer consumer;

    public Recorder(AudioSessionConfig cfg, @NonNull AudioConsumer consumer) {
        this.cfg = cfg;
        this.consumer = consumer;

        recorder = new AudioRecord(AUDIO_SOURCE, cfg.sampleRate, AudioFormat.CHANNEL_IN_MONO, cfg.encoding, cfg.minBufSize(AudioFormat.CHANNEL_IN_MONO));
        encoder = new OpusEncoder();
        encoder.init(cfg.sampleRate, 1, OpusEncoder.OPUS_APPLICATION_VOIP);
    }

    public void shutdown() {
        end();

        encoder.close();
        recorder.release();
    }

    public void start() {
        if (running) {
            return;
        }

        running = true;
        senderThread = Executors.newSingleThreadExecutor();
        senderThread.submit(this::looper);
    }

    private void looper() {

        try {

            recorder.startRecording();


            int encoded;
            short[] inBuf = new short[cfg.bufSize];
            byte[] outBuf = new byte[48];

            while (!Thread.interrupted() && running) {

                recorder.read(inBuf, 0, inBuf.length);
                encoded = encoder.encode(inBuf, cfg.frameSize, outBuf);

                byte[] copyBuf = new byte[outBuf.length];

                System.arraycopy(outBuf, 0, copyBuf, 0, outBuf.length);

                consumer.onAudio(copyBuf);

                Log.v(TAG, "Encoded " + inBuf.length + " bytes to " + encoded + " bytes");
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            recorder.stop();
        }
    }

    public void end() {
        if (!running) {
            return;
        }

        running = false;
        senderThread.shutdownNow();
        try {
            senderThread.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    public interface AudioConsumer {
        void onAudio(byte[] data);
    }
}
