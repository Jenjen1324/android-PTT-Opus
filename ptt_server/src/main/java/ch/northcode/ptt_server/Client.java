package ch.northcode.ptt_server;

import java.net.DatagramPacket;
import java.net.InetAddress;

/**
 * Created by Jens on 04.07.2018.
 */

public class Client {

    private static final int TIMEOUT = 60 * 1000; // 60s

    private InetAddress address;
    private int targetPort;


    private long lastPing;

    public Client(InetAddress address, int targetPort) {
        this.address = address;
        this.targetPort = targetPort;
        this.lastPing = System.currentTimeMillis();
    }

    public DatagramPacket createPacket(byte[] buffer) {
        return new DatagramPacket(buffer, buffer.length, address, targetPort);
    }

    public DatagramPacket createPacket(String payload) {
        byte[] data = payload.getBytes();
        return createPacket(data);
    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof Client) {
            return ((Client) o).address.equals(address) && ((Client) o).targetPort == targetPort;
        }

        return false;
    }

    @Override
    public String toString() {
        return "Client: " + address.getHostAddress() + ":" + targetPort;
    }
}
