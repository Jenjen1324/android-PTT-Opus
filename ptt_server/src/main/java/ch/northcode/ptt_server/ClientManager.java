package ch.northcode.ptt_server;

import java.net.DatagramPacket;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Jens on 04.07.2018.
 */

public class ClientManager {

    private Set<Client> clients = new HashSet<>();

    public Client findOrCreateClient(DatagramPacket packet) {
        Client tmpClient = new Client(packet.getAddress(), packet.getPort());
        for (Client c : clients) {
            if (c.equals(tmpClient)) {
                return c;
            }
        }

        clients.add(tmpClient);
        return tmpClient;
    }

    public Set<Client> getExcludingList(Client c) {
        Set<Client> set = new HashSet<>();
        for(Client cl : clients) {
            if (cl != c) {
                set.add(cl);
            }
        }

        return set;
    }

}
