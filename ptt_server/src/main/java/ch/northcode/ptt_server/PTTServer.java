package ch.northcode.ptt_server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class PTTServer {

    private DatagramSocket socket;

    private boolean running = false;

    private byte[] buffer;

    private ClientManager clientManager;

    public static void main(String[] args){

        try {
            new PTTServer(160, 8884)
            .start();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public PTTServer(int bufferSize, int port) throws SocketException {
        socket = new DatagramSocket(port);
        buffer = new byte[bufferSize];
        clientManager = new ClientManager();
    }

    public void start() throws IOException {
        running = true;
        run();
    }

    private void run() throws IOException {
        while (running) {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            Client client = clientManager.findOrCreateClient(packet);

            String dataStr = new String(packet.getData());

            if (dataStr.startsWith("REG")) {
                System.out.println("Registration received from: " + client.toString());
                socket.send(client.createPacket("OK"));
            } else {

                for (Client c : clientManager.getExcludingList(client)) {
                    System.out.println("Sending packet to " + client.toString());
                    socket.send(c.createPacket(packet.getData()));
                }
            }

//            System.out.println("Received something: " + packet.toString());
        }
    }

}
